const { src, dest, watch, series } = require('gulp')
const sass = require('gulp-sass')(require('sass'))

function buildStyles() {
  return src('src/styles/main.scss')
    .pipe(sass())
    .pipe(dest('src/styles/css'))
}

function watchStyles() {
  watch(['src/styles/main.scss'], buildStyles)
}

exports.default = series(buildStyles, watchStyles)