import { HashRouter, Routes, Route } from 'react-router-dom'
import React from 'react'
import HomePage from './routes/HomePage'
import VimPage from './routes/VimPage';
import BashPage from './routes/BashPage';
import CSSPage from './routes/CSSPage'
import GitPage from './routes/GitPage'
import ReactPage from './routes/ReactPage'
import JavaScriptPage from './routes/JavaScriptPage';
import ColorButton from './routes/react-notes/ColorButton'
import ReactTesting from './routes/react-notes/ReactTesting'
import ReactSamples from './routes/react-notes/ReactSamples'
import Flex from './routes/css-notes/Flex'
import Grid from './routes/css-notes/Grid'
import Sass from './routes/css-notes/Sass'
import Gulp from './routes/css-notes/Gulp'
import Cypress from './routes/react-notes/Cypress';
import Jest from './routes/react-notes/Jest';
import Reducer from './routes/react-notes/Reducer';
import ReducerTodos from './routes/react-notes/ReducerTodos';
import Counter from './routes/react-notes/Counter';
import Algorithms from './routes/Algorithms';
import Messaging from './routes/Messaging';
import Nifi from './routes/messaging-notes/Nifi';
import Python from './routes/PythonPage';
import PythonTesting from './routes/python-notes/PythonTesting'
import Pytest from './routes/python-notes/Pytest';

function App() {
  return (
    <main>
      <HashRouter>
        <Routes>
          <Route exact path="/" element={<HomePage />}>
            {/* nested routes - use later */}
            {/* <Route exact path="/vim" element={<VimPage/>} />
            <Route exact path="bash" element={<BashPage/>} /> */}
          </Route>
          <Route exact path="/vim" element={<VimPage />} />
          <Route exact path="/bash" element={<BashPage />} />
          <Route exact path="/css" element={<CSSPage />} />
          <Route exact path="/git" element={<GitPage />} />
          <Route exact path="/javascript" element={<JavaScriptPage />} />
          <Route exact path="/color-button" element={<ColorButton />} />
          <Route exact path="/sass" element={<Sass />} />
          <Route exact path="/flex" element={<Flex />} />
          <Route exact path="/grid" element={<Grid />} />
          <Route exact path="/gulp" element={<Gulp />} />
          <Route exact path="/react" element={<ReactPage />} />
          <Route exact path="/react-testing" element={<ReactTesting />} />
          <Route exact path="/react-samples" element={<ReactSamples />} />
          <Route exact path="/reducer" element={<Reducer />} />
          <Route exact path="/reducer-todos" element={<ReducerTodos />} />
          <Route exact path="/reducer-counter" element={<Counter />} />
          <Route exact path="/cypress" element={<Cypress />} />
          <Route exact path="/jest" element={<Jest />} />
          <Route exact path="/algorithms" element={<Algorithms />} />
          <Route exact path="/messaging" element={<Messaging />} />
          <Route exact path="/nifi" element={<Nifi />} />
          <Route exact path="/python" element={<Python />} />
          <Route exact path="/python-testing" element={<PythonTesting />} />
          <Route exact path="/pytest" element={<Pytest />} />

        </Routes>
      </HashRouter>
    </main>
  );
}

export default App;
