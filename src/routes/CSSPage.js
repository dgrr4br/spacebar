import { Link } from 'react-router-dom'
import React from 'react'


export default function CSSPage() {
    return (

    <main className="container">
        <header>
            <h2>CSS<br />
            </h2>
        </header>
        <section class="css-container">
                <article className="card">
                    <img className="card__svg" src="https://www.vectorlogo.zone/logos/sass-lang/sass-lang-icon.svg" alt=""/>
                    <Link to="/sass">Sass</Link>
                </article>
                <article className="card">
                    <img className="card__svg" src="https://img.icons8.com/emoji/48/000000/flexed-biceps.png" alt="a flexing bicep"/>
                    <Link to="/flex">Flex</Link>
                </article>
                <article className="card">
                    <img className="card__svg" src="https://img.icons8.com/ios/50/000000/grid.png" alt="a grid"/>
                    <Link to="/grid">Grid</Link>
                </article>
                <article className="card">
                    <img className="card__svg" src="https://www.vectorlogo.zone/logos/gulpjs/gulpjs-ar21.svg" alt="gulp icon"/>
                    <Link to="/gulp">Gulp</Link>
                </article>
        </section>
    </main>
    )
}