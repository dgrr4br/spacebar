import React from 'react'


export default function GitPage() {
    return (

    <main>
        <header class="major">
            <h2>Git<br />
            </h2>
        </header>
        <section class="skill">
            <article class="skill__skill-item">
                <button class="skill__skill-item--button">
                    <h3 class="skill__skill-item--heading">Actual Git Content</h3>
                    <img class="skill__skill-item--svg" src="https://www.vectorlogo.zone/logos/typescriptlang/typescriptlang-icon.svg" alt=""/>
                </button>
            </article>
            <pre>


                {/* block__element--modifier {
                    background-color: $light-color;
                } */}
            </pre>
        </section>

    </main>

    )
}