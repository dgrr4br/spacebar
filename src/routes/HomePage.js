import { Link } from 'react-router-dom'
import React from 'react'

export default function HomePage() {
    return (
        
    <main className='container'>
        <header>
            <h2>Skills<br />
            </h2>
        </header>
        <section className='home-container'>
            <article className="card">
                <img className="card__svg" src="https://www.vectorlogo.zone/logos/vim/vim-icon.svg" alt=""/>
                <Link to="vim">Vim</Link>
            </article>
            <article className="card">
                <img className="card__svg" src="https://www.vectorlogo.zone/logos/gnu_bash/gnu_bash-icon.svg" alt=""/>
                <Link to="bash" >Bash</Link>
            </article>           
            <article className="card">
                <img className="card__svg" src="https://www.vectorlogo.zone/logos/w3_css/w3_css-icon.svg" alt=""/>
                <Link to="/css" >CSS</Link>
            </article>             
            <article className="card">
                <img className="card__svg" src="https://www.vectorlogo.zone/logos/git-scm/git-scm-icon.svg" alt=""/>
                <Link to="/git" >Git</Link>
            </article>  
            <article className="card">
                <img className="card__svg" src="https://www.vectorlogo.zone/logos/reactjs/reactjs-icon.svg" alt="react"/>
                <Link to="/react" >React</Link>
            </article> 
            <article className="card">
                <img className="card__svg" src="https://www.vectorlogo.zone/logos/javascript/javascript-icon.svg" alt="javascript"/>
                <Link to="/javascript" >JavaScript</Link>
            </article>
            <article className="card">
                <img className="card__svg" src="" alt=""/>
                <Link to="/messaging" >Messaging</Link>
            </article>
            <article className="card">
                <img className="card__svg" src="" alt=""/>
                <Link to="/python" >Python</Link>
            </article>
        
        </section>
        {/* outlet not necessary for sibling routes - use when nested */}
        {/* <Outlet /> */}

    </main>
    
    )
}
