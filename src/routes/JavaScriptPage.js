import { Link } from 'react-router-dom'
import React from 'react'


export default function JavaScriptPage() {
    return (

        <main className="container">
            <header>
                <h2>JavaScript<br />
                </h2>
            </header>
            <section class="javascript-container">
                <article className="card">
                    <img className="card__svg" src="https://www.vectorlogo.zone/logos/eslint/eslint-icon.svg" alt="es lint icon" />
                    <Link to="/eslint">ES Lint</Link>
                </article>
                <article className="card">
                    <img src="" alt="" className="card_svg" />
                    <Link to={"/aoc-js"}></Link>
                </article>

            </section>
        </main>
    )
}