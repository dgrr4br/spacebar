import React from 'react'
import { Link } from 'react-router-dom'

function Messaging() {
  return (
        <main className="messaging-container">
            <header>
                <h2>Messaging<br />
                </h2>
            </header>
            <section className="messaging-container">
                <article className="card">
                    <img className="card__svg" src="" alt="" />
                    <Link to="/jms">JMS</Link>
                </article>
                <article className="card">
                    <img className="card__svg" src="" alt="" />
                    <Link to="/nifi">Nifi</Link>
                </article>

            </section>
            <section>
                <h1>Java Message Service - course notes</h1>
                <h2>Course link: <a href="https://digitalu.udemy.com/course/java-message-service-jms-fundamentals">digitalu.udemy.com</a></h2>
                <ul>Notes
                    <li>Why Messaging?

                        <ul>
                        <li>Heterogenous integration</li>
                        <li>Loosely coupled</li>
                        <li>Reduce system bottlenecks</li>
                        <li>Scalability</li>
                        </ul>
                    </li>
                    <li>What is JMS
                        <ul>
                        <li>
                            <p>An API that allows you switch out messaging vendors</p>
                            <p>Devs can learn just one API and then interact with any messaging servers that implements the JMS standard</p>
                        </li>
                        </ul>
                    </li>
                    <li>Two Messaging Models
                        <ul>
                        <li>Point to Point
                            <ul>
                            <li>Queue</li>
                            <li>Message consumed only once</li>
                            <li>JMS producer is responsible for ensureing msg is consumed by only one application</li>
                            <li>Supports both Async fire and forget and Synchronous req/res messaging (the latter involves consumer putting res on diff queue</li>
                            </ul>
                        </li>
                        <li>Pub/Sub
                            <ul>
                            <li>Topic</li>messaging
                            <li>Messages recieved by multiple subsribers</li>
                            <li>Push model - messages auto broadcasted by JMS producer to consumers w/o them having to request from topic</li>
                            </ul>
                        </li>
                        </ul>

                    </li>
                    <li>JMS provider is the messaging server that implements the JMS specification
                        <ul>
                        <li>This is the Message oriented middleware (MOM)</li>
                        <li>Apache ActiveMQ Artemis is that JMS provider that will be used to create JMS clients (it implements the JMS specification)</li>
                        <li>Install it, create a broker (a JMS provider), run it</li>
                        <li>Create "administered" objects (Connection factory, queue, topic) and put them in the JNDI registry</li>
                        <li>JMS clients can access these objects through JNDI (Java Naming and Directory Interface)</li>
                        </ul>
                    </li>
                    <li>Spring Tool</li>
                    <ul>
                        <li>config a JDK for spring tool suite</li>
                        <li>Window ... Preferences</li>
                        <li>search for jre</li>
                        <li>installed JREs</li>
                        <li>edit</li>
                        <li></li>
                    </ul>
                    <li>Create Artemis messaging broker that will be used as JMS provider/broker
                        <ul>Go to artemis bin dir
                            <li>./artemis create /path/where/you/want/name_of_broker</li>
                            <li>admin:admin and Y</li>
                            <li>run broker by going to it and running <code>./artemis run</code></li>
                            <li>in etc folder, broker.xml has configuration details for queues and other stuff</li>
                        </ul>
                    </li>
                    <li>JMS 1.x
                        <ul>
                            <li>ConnectionFactory
                                <ul>
                                    <li>provided by JMS provider (e.g. ActiveMQ) and put into JNDI registry</li>
                                    <li>this and Destination creation (see below) are very first steps in and JMS client code or application</li>
                                </ul>
                            </li>
                            <li>Destination
                                <ul>
                                    <li>also provided by JMS provider (e.g. ActiveMQ) and put into JNDI registry</li>
                                    <li>could be queue or topic</li>
                                </ul>
                            </li>
                            <li>Connection
                                <ul>
                                    <li>created from the ConnectionFactory</li>
                                </ul>
                            </li>
                            <li>Session
                                <ul>
                                    <li>created from the Connection</li>
                                    <li>a 'unit of work' in JMS</li>
                                    <li>can create any number of sessions</li>
                                    <li>with a session you create Message, MessageProducer, MessageConsumer</li>
                                    <li>if Destination is queue, MessageProducer is a QueueProducer, if topic TopicProducer</li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    <li>Create Maven project so we can create a JMS client
                        <ul>
                            <li>
                                Create project itself
                                <ul>
                                    <li>File ... new ... other ... search for maven</li>
                                    <li>maven project ... next ... next </li>
                                    <ul>
                                        <li>Select an archetype</li>
                                        <li>groupid: org.apache.maven.archetypes</li>
                                        <li>artifactid: maven-archetype-quickstart</li>
                                    </ul>
                                    <li>group id: com.whatever.jms</li>
                                    <li>artifact id: whatever (keep in mind you are going to use a project to create a lot of jms clients </li>
                                    <li>Finish</li>
                                    <li>delete root/src/main/java/com.whatever.jms.root</li>
                                    <li>^^^do the same for test</li>
                                </ul>
                            </li>
                            
                            <li>Add Artemis client/maven dependency to the pom.xml
                                <ul>
                                    <li>open pom.xml</li>
                                    <li>delete junit dependency</li>
                                    <li>google artemis jms client all maven dependency</li>
                                    <li>https://mvnrepository.com/artifact/org.apache.activemq/artemis-jms-client-all/2.2.0</li>
                                    <li>click last breadcrumb to get latest version</li>
                                    <li>https://mvnrepository.com/artifact/org.apache.activemq/artemis-jms-client-all</li>
                                    <li>copy the Maven dependency code, paste in pom.xml dependencies tag, and delete the comment with ctrl + d, ctrl + shift + f to format and save (spring tool suite will pull the dep and add it to the project (see it under maven dependencies))</li>
                                </ul>
                            </li>
                            <li>jndi.properties
                                <ul>
                                <li>create the file
                                    <ul>
                                    <li>under project root, create new source folder</li>
                                    <li>src/main/resources</li>
                                    <li>Finish</li>
                                    <li>add a jndi.properties file to the resources folder</li>
                                    <li>initialize ActiveMQ initial context factory, the connection factory and the queues and topics with their jndi names</li>
                                    </ul>
                                </li>
                                <li>add first property: initial context factory
                                    <ul>
                                        <li>
                                            <pre><code>
                                                java.naming.factory.initial=
                                            </code></pre>
                                            <p>kb shortcut: ctrl + shift + t to search class names</p>
                                        </li>
                                        <li>in the class finder, find ActiveMQInitialContextFactory</li>
                                        <li>when it opens, scroll to top and copy package name</li>
                                        <li>scroll up </li>
                                        <li>the above says what should be used as a jndi client</li>
                                    </ul>
                                </li>
                                </ul>
                            </li>
                        </ul>
                        <ul>
                            <li>Create P</li>
                        </ul>
                    </li>
                    <li>Create our first queue
                        <ul>
                            <li>(ctrl + 1 to fix errors)
                                <pre><code>{`
                                    package com.digucourse.jms.basics;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

public class FirstQueue {
	
	public static void main(String[] args) {
		
		// this context the root of the jndi registry
		InitialContext initialContext = null;
		
		try {
			// all this is for JMS 1.x API, 2.0 is much simpler
			initialContext = new InitialContext(); // when we create instance here, it uses info from jndi.properties
			// now retrieve the connection factory from the jndi registry ("look it up")
			ConnectionFactory cf = ((ConnectionFactory) initialContext.lookup("ConnectionFactory")); // param is whatever we named it in jndi.properties
			Connection connection = cf.createConnection(); // will throw the JMSException below if bad
			Session session = connection.createSession();
			Queue queue = (Queue) initialContext.lookup("queue/myQueue"); // lookup returns obj so need to cast
			MessageProducer producer = session.createProducer(queue); // param is the queue that we have defined in jndi.properties (jndi registry)
			TextMessage message = session.createTextMessage("I am the creator of my destiny");
			producer.send(message);
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
}`}
                                    </code></pre>
                            </li>
                        </ul>
                    </li>
                    <li>Add a consumer to above code:
                        <br></br>
                        <pre><code>
                            {`
package com.digucourse.jms.basics;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Queue;
import javax.jms.Session;
import javax.jms.TextMessage;

public class FirstQueue {
	
	public static void main(String[] args) {
		
		// this context is the root of the jndi registry
		InitialContext initialContext = null;
		
		Connection connection = null;
		
		// both of above need to be declared above the try in order to be closed in finally
		
		try {
			// all this is for JMS 1.x API, 2.0 is much simpler
			initialContext = new InitialContext(); // when we create instance here, it uses info from jndi.properties
			// now retrieve the connection factory from the jndi registry ("look it up")
			ConnectionFactory cf = ((ConnectionFactory) initialContext.lookup("ConnectionFactory")); // param is whatever we named it in jndi.properties
			connection = cf.createConnection(); // will throw the JMSException below if bad
			Session session = connection.createSession();
			Queue queue = (Queue) initialContext.lookup("queue/myQueue"); // lookup returns obj so need to cast
			MessageProducer producer = session.createProducer(queue); // param is the queue that we have defined in jndi.properties (jndi registry)
			TextMessage message = session.createTextMessage("I am the creator of my destiny");
			producer.send(message);
			
			MessageConsumer consumer = session.createConsumer(queue);
			connection.start(); // producers don't need to be started but consumers do
			TextMessage messageReceived = (TextMessage) consumer.receive(5000);
			System.out.println("messge Received: " + messageReceived.getText());
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (JMSException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally { // make sure you are closing initial context and the connection
			if(initialContext != null) {
				try {
					initialContext.close();
				} catch (NamingException e) {
					e.printStackTrace();
				}
			}
			if(connection != null) {
				try {
					connection.close();
				} catch (JMSException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
                            `}
                            </code></pre>
                    </li>

                </ul>
            </section>
        </main>  )
}

export default Messaging