import { Link } from 'react-router-dom'
import React from 'react'


export default function PythonPage() {
    return (

        <main className="container">
            <header>
                <h2>Python<br />
                </h2>
            </header>
            <section class="javascript-container">
                <article className="card">
                    <img className="card__svg" src="" alt="" />
                    <Link to="/python-testing">Testing</Link>
                </article>

            </section>
        </main>
    )
}