import { Link } from 'react-router-dom'
import React from 'react'


export default function ReactPage() {
    return (

    <main className="container">
        <header>
            <h2>React<br />
            </h2>
        </header>
        <section class="react-container">
            <article className="card">
                <img className="card__svg" src="https://img.icons8.com/ios/50/000000/router.png" alt="router icon"/>
                <Link to="/react-router">Router</Link>
            </article>
            <article className="card">
                <img className="card__svg" src="https://img.icons8.com/fluency/48/000000/folder-invoices--v2.png" alt="a folder"/>
                <Link to="/react-organization">Organization</Link>
            </article>
            <article className="card">
                <img className="card__svg" src="https://img.icons8.com/ios/50/000000/grid.png" alt="a grid"/>
                <Link to="/react-testing">Testing</Link>
            </article>
            <article className="card">
                <img className="card__svg" src="" alt=""/>
                <Link to="/react-samples">Samples</Link>
            </article>
            <article className="card">
                <img className="card__svg" src="https://www.vectorlogo.zone/logos/javascript/javascript-icon.svg" alt="javascript"/>
                <Link to="/javascript" >JavaScript</Link>
            </article>
            <article className="card">
                <img className="card__svg" src="" alt=""/>
                <Link to="/reducer" >Reducer</Link>
            </article> 
        </section>
    </main>
    )
}