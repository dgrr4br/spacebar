import React from 'react'


export default function VimPage() {
    return (

    <main>
        <header class="major">
            <h2>Vim<br />
            </h2>
        </header>
        <section class="skill">
            <article class="skill__skill-item">
                <button class="skill__skill-item--button">
                    <h3 class="skill__skill-item--heading">Actual Vim Content</h3>
                    <img class="skill__skill-item--svg" src="https://www.vectorlogo.zone/logos/typescriptlang/typescriptlang-icon.svg" alt=""/>
                </button>
            </article>
        </section>

    </main>

    )
}