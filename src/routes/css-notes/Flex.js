import React from 'react'


export default function Flex() {
    return (
        <main className="container">
        <section className="container">
            <pre>
            <code>
{`get flex items to expand and shrink with window
    flex-grow: 1;
    if all items are flex-grow 1 make another item flex-grow: 2
        this will make it grow twice as wide`}
            </code>
            </pre>
<pre><code>
{``}
            </code></pre>
        </section>
        </main>
    )
}