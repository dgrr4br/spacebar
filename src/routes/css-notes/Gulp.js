import React from 'react'


export default function Gulp() {
    return (
        <section class="sass">
            <pre className="pre">
                npm i -g(lobal) sass
                sass --watch scss/style.scss css/style.css
                # scss compiles to css without scss variables

                # usage example:
                $light-color: red;

                {/* block__element--modifier {
                    background-color: $light-color;
                } */}
            </pre>
            <pre className="pre">
                SCSS
                "https://www.youtube.com/watch?v=_a5j7KoflTs&t=219s"
                vscode &gt; extension &gt; live sass compiler
                settings (open settings.json):
                    put compiled css in distribution folder
                gulpfile - a js file that contains functions for gulp to run
                install it - npm install --global gulp-cli
                npm install --save-dev gulp
                npm install sass gulp-sass --save-dev
                gulp --version
                start it with gulp at terminal 
            </pre>
        </section>

    )
}