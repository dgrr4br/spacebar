import React from 'react'

function Nifi() {
  return (
    <main>
        <header>
                <h2>Nifi<br />
                </h2>
            </header>
            <section className="messaging-container">

            </section>
            <section>
                <pre><code>
                    {` 
https://dev.to/markbdsouza/getting-started-with-apache-nifi-42oh

dl page for bin and .asc file: https://dlcdn.apache.org/
    https://www.apache.org/dyn/closer.cgi#verify

commands to check sig:
gpg --import KEYS
gpg --verify nifi-1.15.3-bin.tar.gz.asc nifi-1.15.3-bin.tar.gz 

I dl'd to project/nifi

open tarball:
tar -xvf nifi-1.15.3-bin.tar.gz



run nifi on localhost:8443/nifi
bin/nifi.sh run

credentials in logs/nifi-app.log

for AMQP:
Add Processor
Type: ConsumeAMQP
Add
right-click > Configure

Use: https://nifi.apache.org/docs/nifi-docs/components/org.apache.nifi/nifi-amqp-nar/1.6.0/org.apache.nifi.amqp.processors.ConsumeAMQP/

https://digitalu.udemy.com/course/domain-driven-design-and-microservices/learn/lecture/26079194#overview

ActiveMQ is AMQP (Advanced Message Queuing Protocol) compliant

cannot use ConsumeAMQP with ActiveMQ since ActiveMQ only supports AMQP 1.0 and Nifi's ConsumeAMQP processor only supports AMQP 0.9.1

We will use ConsumeJMS (leverages the JMS API directly) and Apache Qpid Proton library (https://qpid.apache.org/proton/)

Download and install Qpid JMS (AMQP 1.0) (https://qpid.apache.org/download.html)
    (try Qpid Proton later)
`}</code></pre>
            </section>
    </main>
  )
}

export default Nifi