import React from 'react'


export default function Pytest() {
    return (
        <main className="container">
        <section className="container">
            <pre>
            <code>
{`main notes (see example files below):

to make a separate test module:
	make a new file and name it test*.py or *test.py
	import the functions (through their containing modules) that need to be tested
	write a method that has same naming convention as module
	include "assert <test condition>" at the end

to run it:
	pytest <test module name>
	
to run single test:
    pytest <test module name>::<test function>

or in sidebar:
	install test explorer extension
	open command palette (view > command palette or ctrl+shift+p), select test framework
	select directory
`}
            </code>
            </pre>
<pre><code>
                <p>some_module.py</p>

{`"""An example Python module."""

from typing import List


def total(xs: List[float]) -> float:
    """Total returns the sum of xs."""
    return 0.0


def join(xs: List[int], delimiter: str) -> str:
    return ""
`}
            </code></pre>
            <pre><code>
                <p>test_some_module.py</p>
            {`"""An example of a test module in pytest."""

from some_module import total

# arrow indicates what the function returns


def test_total_empty() -> None:
    # if what I assert is true, test passes; else fails
    assert total([]) == 1.0
`}
                  </code></pre>
<p>Better example</p>
<pre><code>
{`def helper_function(*args):
    # do something
    return something


def main_function_to_test(some_args):
    # do something
    a = helper_function(args)
    # do something
    return something


print(main_function_to_test(args))`}
      </code></pre>
      <pre><code>
{`input_data = [
    (
        "emptyInput",
        [],
        []
    ),
    (
        "second input",
        [
            {
                "input key 1": "input value 1",
            }
        ],
        [
            {
                "expected key 1": "expected value 1",
            }
        ]
    ),
]
`}
      </code></pre>
            <pre><code>
{`
import file_that_has_function_to_test

from input_data import input_data
import pytest

@pytest.mark.parametrize("test_name,test_input,test_output", input_data)
def test_main_function_to_test(test_name, test_input, test_output) -> None:
    print(f'test name: \n{test_name}\n\n')
    assert file_that_has_function_to_test.main_function_to_test(test_input) == test_output
`}
      </code></pre>
      <ul>
        <li> 
          <ul>
          </ul>
        </li>
      </ul>
        </section>
        </main>
    )
}