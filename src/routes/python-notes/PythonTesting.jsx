import { Link } from 'react-router-dom'
import React from 'react'


export default function PythonTesting() {
    return (

    <main className="python-testing-container">
        <header>
            <h2>Python Testing<br />
            </h2>
        </header>
        <section class="react-flex-container">
          <article className="card">
              <img className="card__svg" src="" alt=""/>
              <Link to="/pytest">Pytest</Link>
          </article>
          
        </section>
        <p>In the simplest terms, a test is meant to look at the result of a particular behavior, and make sure that result aligns with what you would expect. Behavior is not something that can be empirically measured, which is why writing tests can be challenging.</p>
        <p>“Behavior” is the way in which some system acts in response to a particular situation and/or stimuli. But exactly how or why something is done is not quite as important as what was done.</p>
        <p>You can think of a test as being broken down into four steps:</p>
        <ol>
            <li>Arrange</li>
            <li>Act</li>
            <li>Assert</li>
            <li>Cleanup</li>
        </ol>
        <p><strong>Arrange</strong>is where we prepare everything for our test. This means pretty much everything except for the “<strong>act</strong>”. It&#8217;s lining up the dominoes so that the <strong>act</strong> can do its thing in one, state-changing step. This can mean preparing objects, starting/killing services, entering records into a database, or even things like defining a URL to query, generating some credentials for a user that doesn&#8217;t exist yet, or just waiting for some process to finish.</p>

        <p>Act is the singular, state-changing action that kicks off the behavior we want to test. This behavior is what carries out the changing of the state of the system under test (SUT), and it&#8217;s the resulting changed state that we can look at to make a judgement about the behavior. This typically takes the form of a function/method call.</p>
        <p>Assert is where we look at that resulting state and check if it looks how we&#8217;d expect after the dust has settled. It&#8217;s where we gather evidence to say the behavior does or does not aligns with what we expect. The assert in our test is where we take that measurement/observation and apply our judgement to it. If something should be green, we&#8217;d say assert thing == "green".</p>
        <p>Cleanup is where the test picks up after itself, so other tests aren’t being accidentally influenced by it.</p>

    
        
    </main>
    )
}