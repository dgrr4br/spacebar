import React, { useReducer } from 'react'

// reducer read what action is and changes state accordingly
function reducer(state, action) {
  switch(action.type) {
    case ACTIONS.INCREMENT:
      return { count: state.count + 1};
    case ACTIONS.DECREMENT:
      return { count: state.count - 1};
    default:
      return state;
  }
}

const ACTIONS = {
  INCREMENT: "increment",
  DECREMENT: "decrement"
}

export default function Counter() {
  const [state, dispatch] = useReducer(reducer, { count: 0 });

  function increment() {
    dispatch({ type: ACTIONS.INCREMENT })
  }

  function decrement() {
    dispatch({ type: ACTIONS.DECREMENT })
  }

  return (
    <div>
      <div>count: {state.count}</div>    
      <button onClick={increment}>increment</button>
      <button onClick={decrement}>decrement</button>
    </div>
  )
}
