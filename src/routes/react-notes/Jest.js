import React from 'react'


export default function Jest() {
    return (
        <main className="container">
        <section className="container">
            <pre>
            <code>
{`What is Jest?
  Test runner
    - finds tests
    - runs tests
    - determines whether tests pass or fail`}
            </code>
            </pre>
<pre><code>
{`all set up if you do create-react-app
first test is already made in App.test.js
npm test
a to run tests
q to quit watch mode`}
            </code></pre>
            <pre><code>
            {`import { render, screen } from '@testing-library/react';
import App from './App';

test('renders learn react link', () => {
  render(<App />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});`}
                  </code></pre>
<pre><code>
{`let's break down this syntax:
  render
    - create virtual DOM for argument JSX
    - access virtual DOM via 'screen' global object
  screen.getByText()
    - find an element based on what text it displays
    - argument is a regex (could also use an actual string though)
    - i means case insensitive
  expect().toBeInTheDocument()
    - assertion -- causes test to succeed or fail
    - expect is a global in jest
    - argument is what jest is asserting against
    - toBeInDocument is a matcher; it's what the assertion 'type' is
        - this matcher comes from Jest-DOM
        - if matcher takes argument, it's to refine the matcher`}
      </code></pre>
<pre><code>
{`more assertion examples:
expect(element.textContent).toBe('Hello');
  element would be something defined on previous line (using a screen method)
expect(elementsArray).toHaveLength(7)`}
      </code></pre>
      <ul>
        <li>jest-dom 
          <ul>
            <li>Comes with create-react-app</li>
            <li>setupTests.js imports it before each test</li>
            <li>dom-based matchers (unlike the above) are <code>
              toBeVisible() or toBeChecked()</code></li>
          </ul>
        </li>
      </ul>
        </section>
        </main>
    )
}