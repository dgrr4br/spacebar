import { Link } from 'react-router-dom'
import React from 'react'


export default function ReactSamples() {
    return (

    <main className="container">
        <header>
            <h2>React Samples<br />
            </h2>
        </header>
        <section class="react-container">
            <article className="card">
                <img className="card__svg" src="" alt=""/>
                <Link to="/color-button">Color Button</Link>
            </article>
            <article className="card">
                <Link to="/reducer-counter">Counter</Link>
            </article>
            <article className="card">
              <Link to="/reducer-todos">Reducer Todos</Link>
          </article>
        </section>
    </main>
    )
}