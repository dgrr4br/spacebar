import { Link } from 'react-router-dom'
import React from 'react'


export default function ReactTesting() {
    return (

    <main className="react-testing-container">
        <header>
            <h2>React<br />
            </h2>
        </header>
        <section class="react-flex-container">
          <article className="card">
              <img className="card__svg" src="https://www.vectorlogo.zone/logos/jestjsio/jestjsio-icon.svg" alt="jest icon"/>
              <Link to="/jest">Jest</Link>
          </article>
          <article className="card">
              <img className="card__svg" src="https://www.vectorlogo.zone/logos/sass-lang/sass-lang-icon.svg" alt=""/>
              <Link to="/cypress">Cypress</Link>
          </article>
          <article className="card">
              <img className="card__svg" src="https://miro.medium.com/max/72/1*JZl-TXoSiG0VmYn3qWLdTA.png" alt=""/>
              <Link to="/enzyme">Enzyme</Link>
          </article>
          <article className="card">
              <img className="card__svg" src="https://img.icons8.com/ios/50/000000/grid.png" alt=""/>
              <Link to="/react-testing-library">RTL</Link>
          </article>
        </section>
        <section className="two-col-grid">
          <h3 className='two-col-grid__header two-col-grid__header--unit'>Unit Testing</h3>
          <h3 className='two-col-grid__header two-col-grid__header--functional'>Functional Testing</h3>
          <ul className="ul-unit">
            <li>Isolated: mock dependencies, test internals</li>
            <li>Pro: very easy to pinpoint failures</li>
            <li>Con: Further from how users interact with software</li>
            <li>Con: More likely to break with refactoring</li>
          </ul>
          <ul className="ul-functional">
            <li>Include all relevant units, test behavior</li>
            <li>Pro: Close to how users interact with software</li>
            <li>Pro: Robust tests</li>
            <li>Con: More difficult to debug failing tests</li>
          </ul>
        </section>
        <p>RTL emphasizes functional testing</p>
        <h1>Mocking and testing API</h1>
        <h2>Mock Service Worker setup</h2>
        <pre><code>
        {`npm install msw
create handlers // determine what's returned for a url
create test server
make sure test server is listening during tests and intercepting calls
reset server handlers after each test
    https://mswjs.io/docs/getting-started/mocks/rest-api
    https://github.com/bonnie/udemy-TESTING-LIBRARY/tree/main/sundae-server
    
// src/mocks/handlers.js
import { rest } from 'msw'`}

              </code></pre>
        <h2>Mock Service Worker handler</h2>
        <pre><code>
        {`rest.get('http://localhost:3030/scoops', (req, res, ctx) => {})
^^^ similar to express
break down of syntax:
    rest: handler type (can be rest or graphql)
    get: method to mock
    full url to mock
    response resolver function
      req: request object
      res: function to create response
      ctx: utility to build response
      https://mswjs.io/docs/basics/response-resolver
sample handler:
import { rest } from 'msw';

export const handlers = [
  rest.get('http://localhost:3030/scoops', (req, res, ctx) => {
    return res(
      ctx.json([
        { name: 'Chocolate', imagePath: '/images/chocolate.png' },
        { name: 'Vanilla', imagePath: '/images/vanilla.png' },
      ])
    );
  }),
];`}

              </code></pre>
        <h2>msw integrate</h2>
        <pre><code>
        {`https://mswjs.io/docs/getting-started/integrate/nodecreate src/mocks/server.js
import { setupServer } from 'msw/node';
import { handlers } from './handlers';

// This configures a request mocking server with the given request handlers.
export const server = setupServer(...handlers);

configure create-react-app so that msw will intercept the nw request and return the responses we established in handlers
`}

              </code></pre>
        <p></p>
    </main>
    )
}