import { Link } from 'react-router-dom'
import React from 'react'


export default function Reducer() {
    return (

    <main className="container">
        <header>
            <h2>useReducer<br />
            </h2>
        </header>
        <section class="react-container">
          <article className="card">
              <Link to="/reducer-counter">Counter</Link>
          </article>
          <article className="card">
              <Link to="/reducer-todos">Reducer Todos</Link>
          </article>
          
        </section>
        <p>Abstractly:</p>
        <p>function reducer(state, action) - this has the switches, these switches could have calls to methods that are also defined outside the component</p>
        <p>Component has the rest:</p>
        <p>instantiation of useReducer(reducer, initial value)</p>
        <p>method in onClick whose implementation includes dispatch</p>
    </main>
    )
}