import React, { useState, useReducer } from 'react'

const ACTIONS = {
  ADD_TODO: "add-todo",
  DELETE_TODO: "delete-todo",
  TOGGLE_TODO: "toggle-todo"
}

// reducer read what action is and changes state accordingly
function reducer(todos, action) {
  switch (action.type) {
    case ACTIONS.ADD_TODO:
      return [...todos, newTodo(action.payload.name)]
    case ACTIONS.DELETE_TODO:
      return deleteTodo(todos, action.payload.id);
    case ACTIONS.TOGGLE_TODO:
      return toggleTodo(todos, action.payload.id);
    default:
      return [];
  }
}

function newTodo(name) {
  return { id: Date.now(), name: name, complete: false }
}

function toggleTodo(todos, id) {
  return todos.map(todo => {
    console.log(todo)
    console.log("passed in id: ", id)
    if (todo.id === id) {
      return {...todo, complete: !todo.complete}
    } else {
      return todo
    }
  })
}

function deleteTodo(todos, id) {
  return todos.filter(todo => {
    return todo.id !== id;
  })
}


export default function ReducerTodos() {
  // only one thing in state so we don't need an object; an array will do
  const [todos, dispatch] = useReducer(reducer, []);
  const [name, setName] = useState('')

  function handleSubmit(e) {
    e.preventDefault() // prevent page from refreshing
    dispatch({ type: ACTIONS.ADD_TODO, payload: { name: name }})
    setName('')
  }

  function handleToggle(e, id) {
    console.log("id in handler: ", id)
    dispatch({ type: ACTIONS.TOGGLE_TODO, payload: { id: id }})
  }

  function handleDelete(e, id) {
    dispatch({ type: ACTIONS.DELETE_TODO, payload: { id: id }})
  }


  return (
    <div>
      <form onSubmit={handleSubmit} action="">
        <input type="text" value={name} onChange={e => setName(e.target.value)} />
      </form>
      {todos && todos.map(todo => {
        return <Todo 
          key={todo.id}
          {...todo}
          handleToggle={handleToggle}
          handleDelete={handleDelete}
        >
        </Todo>
      })}
    </div>
  )
}

// child component of ReducerTodos:
function Todo({ id, name, complete, handleToggle, handleDelete }) {
  console.log("id in child: ", id)
  return (
    <>
      <p style={{ color: complete ? 'green': 'red'}}>{name} </p>
      <button onClick={(e) => {handleToggle(e, id)}}>Toggle</button>
      <button onClick={(e) => {handleDelete(e, id)}}>Delete</button>
    </>
  )
}
